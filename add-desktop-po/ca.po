# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Eduard Selma <selma@tinet.cat>, 2015
msgid ""
msgstr ""
"Project-Id-Version: antix-development\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-04-13 13:55+0300\n"
"PO-Revision-Date: 2014-03-14 10:48+0000\n"
"Last-Translator: Eduard Selma <selma@tinet.cat>, 2015\n"
"Language-Team: Catalan (http://www.transifex.com/anticapitalista/antix-development/language/ca/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ca\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: add-desktop.sh:15
msgid "Item Name:"
msgstr "Nom de l'ítem:"

#: add-desktop.sh:15
msgid "My App"
msgstr "Nova aplicació "

#: add-desktop.sh:15
msgid "Item Icon:FL"
msgstr "Icona de l'ítem:FL "

#: add-desktop.sh:15
msgid "Item Category:CB"
msgstr "Categoria de l'ítem:CB"

#: add-desktop.sh:15
msgid ""
"Application|AudioVideo|Development|Education|Game|Graphics|Network|Office|Settings|System|Utility"
msgstr "Aplicació|Multimèdia|Desenvolupament|Educació|Jocs|Gràfics|Internet|Oficina|Preferències|Sistema|Utilitats"

#: add-desktop.sh:15
msgid "Item Command:"
msgstr "Ordre de l'ítem: "

#: add-desktop.sh:15
msgid "Item Location:CB"
msgstr "Ubicació de l'ítem:CB "

#: add-desktop.sh:15
msgid "1) Application Menu|2) Desktop Shortcut|3) Personal"
msgstr "1)menú d'aplicacions|2) Drecera d'escriptori|3) Personal"

#: add-desktop.sh:15
msgid "File Name:"
msgstr "Nom del fitxer: "

#: add-desktop.sh:15
msgid "Launch in Terminal:CHK"
msgstr "Executa des del Terminal:CHK"

#: add-desktop.sh:15
msgid "add-desktop"
msgstr "Add-desktop"
