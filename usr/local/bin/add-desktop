#!/bin/bash
#Name: add-desktop
#Version: 1.4
#Author: Dave (david@daveserver.info)
#Purpose: Make and add .desktop files to the desktop, applications menu, or personal menu
#License: gplv3

TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=add-desktop

function usage() {
    cat<<Usage
Usage: $0 [options] 

Add .desktop file to the desktop, personal, or the user's applications menu

Options:
    -h --help     Show this help
    -a --add    Add an item given the following information. Format:
                Name|Icon|Category|Exec|Location '1) Application Menu|2) Desktop Shortcut|3) Personal'|File Name|Terminal (True/False)|
    -e --edit   Edit / copy an item given the following information. Format:
                Name|Icon|Category|Exec|Location '1) Application Menu|2) Desktop Shortcut|3) Personal'|File Name|Terminal (True/False)|
    
Usage
}

function checkcancel {
    CANOK="$?";
    case "$CANOK" in
        0) true ;;
        1) exit ;;
        *) 
            echo $"Neither option or an unknown option was selected. Exiting.";
            exit ;;
    esac
}

function GetDesktopInformation() {
    if [ ! -z "$edit_info" ]; then 
        ${IFS+"false"} && unset oldifs || oldifs="$IFS"
        IFS=:
        edit_info=$(echo "$edit_info" |tr '|' ':')
        edit_info=($edit_info)
        ${oldifs+"false"} && unset IFS || IFS="$oldifs"
        local name=${edit_info[0]}
        local icon=${edit_info[1]}
        local command=${edit_info[2]}
        local fname=`echo "${edit_info[3]}" |cut -d "." -f1`
        local categories=${edit_info[4]}
        local terminal=${edit_info[5]}
        app_info=$(yad --form --field=$"Item Name:" "$name" --field=$"Item Icon:FL" "$icon" --field=$"Item Category:CB" "$categories|Application|AudioVideo|Development|Education|Game|Graphics|Network|Office|Settings|System|Utility" --field=$"Item Command:" "$command" --field=$"Item Location:CB" $"1) Application Menu|2) Desktop Shortcut|3) Personal" --field=$"File Name:" "$fname" --field=$"Launch in Terminal:CHK" "$terminal" --title=$"add-desktop" --item-separator="|"); 
        unset edit_info
    else
        app_info=$(yad --form --field=$"Item Name:" $"My App" --field=$"Item Icon:FL" "/usr/share/pixmaps" --field=$"Item Category:CB" $"Application|AudioVideo|Development|Education|Game|Graphics|Network|Office|Settings|System|Utility" --field=$"Item Command:" "my-app.sh" --field=$"Item Location:CB" $"1) Application Menu|2) Desktop Shortcut|3) Personal" --field=$"File Name:" "my-app" --field=$"Launch in Terminal:CHK" --title=$"add-desktop" --item-separator="|");
    fi
    checkcancel;
}

function WriteFile() {
    Exec="$(echo $app_info |cut -d '|' -f4|cut -d ' ' -f1)"
    if [ ! -x "$(which $Exec)" ]; then
        ans=`yad --image dialog-information --title Alert --button=gtk-ok:0 --text $"Item given for execution is not an executable file or does not exist "`;
        checkcancel;
        $0 && exit;
    fi
    echo "[Desktop Entry]" > $1
    echo "Encoding=UTF-8" >> $1
    echo "Type=Application" >> $1
    echo "Name=$(echo $app_info |cut -d '|' -f1)" >> $1
    echo "Icon=$(echo $app_info |cut -d '|' -f2)" >> $1
    echo "Categories=$CATEGORIES" >> $1
    echo "Exec=$(echo $app_info |cut -d '|' -f4)" >> $1
    TERMINAL=$(echo $app_info |cut -d '|' -f7);
    TERMINAL=${TERMINAL,,};
    echo "Terminal=$TERMINAL" >> $1
}

function UpdateMenus() {
    case $1 in
        0) true ;;
        1)  ans=`yad --image dialog-question --title Alert --button=gtk-yes:0 --button=gtk-no:1 --text $"Disable the automatic applications menu and utilize custom user applications menu?"`;
            checkcancel;
            rm $HOME/.$wm/menu-applications;
            desktop-menu --write-out;
            ;;
        2) desktop-menu --menu-file "/etc/xdg/menus/TCM-MENU.menu" --write-out --write-out-file personal ;;
    esac
}

disp=${DISPLAY#:}
disp=${disp%.[0-9]}
code=$(cat $HOME/.desktop-session/desktop-code.$disp)
wm=${code#*-}
im=${code%-$wm}

while [ $# -gt 0 -a -n "$1" -a -z "${1##-*}" ]; do
    param=${1#-}
    shift
    case $param in
        -help|h) usage && exit                         ;;
        -add|a) app_info="$1"                          ;;
        -edit|e) edit_info="$@"                 ;;
        *) echo $"Unknown argument: -$param. Exiting." ;;
    esac
done

if [ -z "$app_info" ]; then GetDesktopInformation ; fi
if [ ! -z "$edit_info" ]; then GetDesktopInformation ; fi

FILE_NAME="$(echo $app_info |cut -d '|' -f6).desktop"
case $(echo $app_info |cut -d '|' -f5|cut -d ")" -f1) in
"1") #Application Menu 
    FILE_WRITE_LOCATION="$HOME/.local/share/applications"
    CATEGORIES="$(echo $app_info |cut -d '|' -f3);"
    MENU_UPDATE=1
    ;;
"2") #Desktop Shortcut
    FILE_WRITE_LOCATION="$XDG_DESKTOP_DIR"
    echo "$XDG_DESKTOP_DIR"
    CATEGORIES="$(echo $app_info |cut -d '|' -f3);"
    MENU_UPDATE=0
    ;;
"3") #Task Centric Menu
    FILE_WRITE_LOCATION="$HOME/.local/share/applications/TCM"
    CATEGORIES="X-Personal;"
    MENU_UPDATE=2
    ;;
esac
FILE_WRITE="$FILE_WRITE_LOCATION/$FILE_NAME"

if [ ! -d "$FILE_WRITE_LOCATION" ]; then
    mkdir -p $FILE_WRITE_LOCATION 2>/dev/null
fi
WriteFile "$FILE_WRITE"
UpdateMenus "$MENU_UPDATE"
